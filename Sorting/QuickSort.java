package Sorting;

public class QuickSort {
	public static void main(String[] args) {
		
		int a[] = {48,36,13,52,19,94,24};
		int length = a.length;
		
		QuickSort qs = new QuickSort();
		qs.quickRec(a,0,length-1);
		qs.printArr(a);
		
	}
	
	int partition(int a[],int low,int high)
	{
		int pivot = a[(low+high)]/2;
		while (low<=high) 
		{
			while(a[low]<pivot)
				low++;
			while(a[high]>pivot)
				high++;
			if(low<=high)
			{
				int temp = a[low];
				a[low] = a[high];
				a[high] = temp;
				
				low++;
				high--;
			}
		}
		return low;
	}
	void quickRec(int a[],int low,int high)
	{
		int pi = partition(a,low,high);
		if(low<pi-1)
			quickRec(a,low,pi-1);
		if(pi<high)
			quickRec(a,pi,high);
	}
	void printArr(int a[])
	{
		for(int i:a)
			System.out.print(i+",");
	}
	
}
