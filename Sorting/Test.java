package Sorting;

import java.io.File;

public class Test {

	public static void main(String[] args) {

		// path for directory
		String maindirpath = "C:\\Users\\Atul\\Desktop\\Data-Test";

		// File object
		File maindir = new File(maindirpath);

		if (maindir.exists() && maindir.isDirectory()) {
			// array for files and sub-directories
			// of directory pointed by maindir
			File arr[] = maindir.listFiles();

			System.out.println(" Main directory : " + maindir);

			RecursivePrint(arr, 0, 0); // Calling recursive method
		}
	}

	static void RecursivePrint(File[] arr, int index, int level) {
		// terminate condition
		if (index == arr.length)
			return;

		// tabs for internal levels
		for (int i = 0; i < level; i++)
			System.out.print("\t");

		// for files
		if (arr[index].isFile())
			System.out.println(arr[index].getName());

		// for sub-directories
		else if (arr[index].isDirectory()) {
			System.out.println("[" + arr[index].getName() + "]");

			// recursion for sub-directories
			RecursivePrint(arr[index].listFiles(), 0, level + 1);
		}

		// recursion for main directory
		RecursivePrint(arr, ++index, level);
	}

}
