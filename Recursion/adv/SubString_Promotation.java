package Recursion.adv;

import java.util.Scanner;

public class SubString_Promotation {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tc = sc.nextInt();
		while (tc-- > 0) {
			String ip = sc.next();
			String op = "";
			SubstringPermutation(ip, op);
		}
	}

	static void SubstringPermutation(String s, String ans) {
		if (s.length() == 0) {
			System.out.print(ans+" ");
			return;
		}
		char ch = s.charAt(0);
		String res = s.substring(1);

		SubstringPermutation(res, ans);
		SubstringPermutation(res, ans + ch);
	}

}
