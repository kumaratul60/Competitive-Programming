package com.Mathmetics;

import java.util.Scanner;

public class SieveEracto {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int tc = sc.nextInt();
		while (tc-- > 0) {
			int n = sc.nextInt();
			boolean prime[] = sieve1(n);

			for (int i = 0; i < n; i++)
				System.out.println(i + " " + prime[i]);
		}
	}

	public static void sieve(int n) {
		boolean isPrime[] = new boolean[n + 1];

		isPrime[0] = true;
		isPrime[1] = true;

		for (int i = 2; i * i <= n; i++) {
			for (int j = 2 * i; j <= n; j += i) {
				isPrime[j] = false;
			}
		}
		for (int i = 2; i <= n; i++) // Print all prime numbers
		{
			if (isPrime[i] == true)
				System.out.print(i + " ");
		}
	}

	public static boolean[] sieve1(int n) {
		boolean isPrime[] = new boolean[n + 1];

		// Arrays.fill(isPrime,true);

		isPrime[0] = true;
		isPrime[1] = true;

		for (int i = 2; i * i <= n; i++) {
			for (int j = 2 * i; j <= n; j += i) {
				isPrime[j] = false;
			}
		}
		return isPrime;
	}

}
