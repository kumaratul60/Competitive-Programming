package Arrays;

import java.util.Scanner;

public class Remoove_Deplicate {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while (t-- > 0) {
			int n = sc.nextInt();
			int arr[] = new int[n];
			for (int i = 0; i < n; i++)
				arr[i] = sc.nextInt();

			int an = (removDup(arr, n));
			for (int i = 0; i < an; i++)
				System.out.print(arr[i] + " ");

		}

	}

	static int removDup(int a[], int n) {
		int j = 0;
		for (int i = 0; i < n - 1; i++) {
			if (a[i] != a[i + 1]) {
				a[j++] = a[i];
			}
		}

		a[j] = a[n - 1];

		return j + 1;

	}

}
