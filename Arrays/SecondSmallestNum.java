package Arrays;

import java.util.Scanner;

public class SecondSmallestNum {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int[n];
		for (int i = 0; i < n; i++)
			a[i] = sc.nextInt();
		System.out.println(secSmallest(a, n));
	}

	static int secSmallest(int a[], int n) {

		int smallest = Integer.MAX_VALUE;
		int secondSmallest = Integer.MAX_VALUE;
		for (int i = 0; i < n; i++) {
			if (a[i] == smallest)
				secondSmallest = smallest;
			else if (a[i] < smallest) {
				secondSmallest = smallest;
				smallest = a[i];
			} else if (a[i] < secondSmallest)
				secondSmallest = a[i];
		}
		return secondSmallest;

	}

}
